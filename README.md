## remus_backseat_msgs

These messages were originally defined by Greg Okopal, in his backseat repo:
* https://bitbucket.org/gokopal/backseat
* https://gitlab.com/apl-ocean-engineering/mcneil-auv-lab/greg_backseat

They have been extracted into an independent repository to minimize dependencies
required to analyze log files from the Remus vehicles.

They do NOT follow ROS conventions for standard units; instead, they keep the
same units used in the corresponding RECON message.
